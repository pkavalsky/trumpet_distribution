<?php

namespace Trumpet\Database;

class DatabaseAdapter 
{
    private $_connection;
    private $_logCount = null;
    private $_includeJsTags = true;
    private $_where = null;
    private $_query = "";
    private $_extension = null;
    private $_bindParamReferences = array();
    private $_table = null;
    private $_orderBy = null;
    private $_order = null;
    private $_limit = null;
    private $_limitOffset = null;
    private $_columns = array();
    
    public static $_instance;
   
/**
     * 
     * Constructor
     * 
     * 
     * @param type $connection
     * @param type $mode
     */
    
    public function __construct(\PDO $connection)
    {
        $this->_connection = $connection;
    }
    
    /**
     * 
     * singleton pattern - 1 databaseadapter per 1 database
     * 
     * @param type $dbname
     * @param \PDO $connection
     * @return type
     */
    public static function getInstance($dbname, \PDO $connection)
    {
        if(isset(self::$_instance[$dbname]))
        {
            return self::$_instance[$dbname];
        }
        return self::$_instance[$dbname] = new DatabaseAdapter($connection);
    }
    
    /**
     * rewrites prepared statement and logs query into the web console
     * @param type $statementString
     * @param type $params
     */
    private function logQuery($statementString, $params)
    {
        $rewrittenStatement = $this->rewriteStatement($statementString, $params);
        $this->outputInConsole($rewrittenStatement);
        $this->_logCount--;
        if($this->_logCount === 0)
        {
            $this->_logCount = null;
            $this->_includeJsTags = true;
        }
    }
    
    /**
     * 
     * runs prepared statement
     *
     * 
     * @param type $statementString (with references :someParam1, :someParam2
     * @param type $params assoc array of references => paramvalues someParam1 => value1, someParam2 => value2
     */
    
    public function sqlquery($statementString, $params = null) 
    {
        //log query if desired
        if(isset($this->_logCount))
        {
            $this->logQuery($statementString, $params);
        }
        $this->statementAuditor($statementString);
        $statement = $this->_connection->prepare($statementString);
        $statement->execute($params);
        
        if(isset($this->_logCount))
        {
            $this->outputInConsole("Affected rows: " . $statement->rowCount());
        }
        return $statement;

    }
    
    /**
     * 
     * checks for mistakes in the query string
     * 
     * @param type $statementString
     * @throws \Exception
     */
    private function statementAuditor($statementString)
    {
        if(is_int(strpos($statementString, "DELETE")) && !is_int(strpos($statementString, "WHERE")))
        {
           throw new \Exception("missing WHERE keyword in DELETE statement. Query was not executed.");
        }
        
        if(is_int(strpos($statementString, "UPDATE")) && !is_int(strpos($statementString, "WHERE")))
        {
           throw new \Exception("missing WHERE keyword in UPDATE statement. Query was not executed.");
        }
    }
            
    /**
     * rewrites prepared statement for debugging
     * 
     * @param type $statementString
     * @param type $params
     * @return type
     */
    private function rewriteStatement($statementString, $params = null)
    {
        if (isset($params))
        {
            foreach ($params as $key => $value)
            {
                $statementString =  str_replace(":$key", "'$value'", $statementString);
            }
        }
        return $statementString;
    }
    
    /**
     * outputs escaped string to web console
     * if error true ouptuts text in red
     * 
     * @param type $rewrittenStatement
     * @param type $error
     */
    private function outputInConsole($rewrittenStatement, $error = false)
    {
        $backgroundColor = "#ffff99";
        $fontColor = "blue";
        $messageHeader = "DEBUG";
        if($error)
        {
            $backgroundColor = "#ffff99";
            $fontColor = "red";
            $messageHeader = "ERROR";
        }
        $jsEscaped = addslashes($rewrittenStatement);
        
        $js = "console.log('%c--DB name--: " . $this->_connection->query('select database()')->fetchColumn() . " --$messageHeader--: $jsEscaped', ' color:$fontColor;font-weight:bold; background-color: $backgroundColor;')";
        if(isset($this->_includeJsTags) && $this->_includeJsTags)
        {
            echo '<script>' . $js . '</script>';
        }
        else
        {
            echo $js;
        }
    }
    
    /**
     * determines how many next statements should be shown in web console for debugging
     * Example usage:
     * $databaseHandler->logNextQueries(2)
     * 
     * $databaseHandler->sqlquery(...) //will be shown in web console
     * $databaseHandler->sqlquery(...) //will be shown in web console
     * $databaseHandler->sqlquery(...) //will NOT be shown in web console
     * 
     * @param type $int
     * @param type $includeTags
     */
    public function logNextQueries($int, $includeTags = null)
    {
        $this->_logCount = $int;
        if (isset($includeTags))
        {
            $this->_includeJsTags = $includeTags;
        }
    }
    
    /**
     * 
     * sets table on which query should be ran on
     * Example usage:
     * 
     * $databaseHandler->table('mytable')
     *                 ->select(...)
     *                 ->.....
     * 
     * 
     * can also be used:
     * 
     * $databaseHandler->mytable()
     *                 ->select(...)
     *                 ->....
     * @param type $name
     * @return $this
     * 
     */
    public function table($name)
    {
        $this->_table = $name;
        return $this;
    }
    
    /**
     * 
     * builds statement with SELECT operator
     * 
     * Example usage:
     * $databaseHandler->mytable()
     *                 ->select() 
     *                 ->commit();   SELECT * FROM mytable
     * 
     * $databaseHandler->mytable()
     *                 ->select( array("column1", "column2") ) 
     *                 ->commit();   SELECT column1, column2 FROM mytable
     * @param type $params
     * @return $this
     */
    public function select($params = null)
    {
        $this->_query = "SELECT ";
        if (!isset($params))
        {
             $this->_query .= "*";
        }
        else if(is_array($params))
        {
             $this->_query .= implode(", ", $params);
        }
        else
        {
             $this->_query .= $params;
        }
        
        $this->_query .= " FROM " . $this->_table;
        return $this;
    }
    
    /**
     * 
     * builds statement with UPDATE operator
     * 
     * Example usage:
     * $databaseHandler->mytable()
     *                 ->set( array("column1" => "myvalue1", "column2" => "myvalue2") ) 
     *                 ->commit();   SELECT * FROM mytable
     * 
     * $databaseHandler->mytable()
     *                 ->select( array("column1", "column2") ) 
     *                 ->commit();   SELECT column1, column2 FROM mytable
     * @param type $updateArray
     * @return $this
     */
    
    public function set($updateArray)
    {
        $this->_query = "UPDATE " . $this->_table . " SET ";
        $array = array();
        foreach ($updateArray as $column => $value)
        {
            //initialize counter
            if(!isset($x))
            {
                $x = 0;
            }
            $array[] = "$column= :$column" . $x;
            $this->_bindParamReferences["$column" . "$x"] = $value;
            $x++;
        }
        $this->_query.= implode(", ", $array);
        return $this;
    }
    
    /**
     * 
     * builds statement using DELETE operator
     * Example usage:
     * 
     * $databaseHandler->mytable()
     *                 ->delete()
     *                 ->where(array("column1" => "myvalue1", "column2" => "myvalue2");
     *                 
     * @return $this
     */
    public function delete()
    {
        $this->_query = "DELETE FROM " . $this->_table;
        return $this;
    }
    
    /**
     * 
     * puts together extension of statement
     * 
     * @return string
     */
    private function getExtension()
    {
        $extension = "";
        //format array
        if(is_array($this->_where))
        {
            $extension .= " WHERE " . implode(" AND ", $this->_where);
        }
        
        if(isset($this->_orderBy))
        {
            $extension .= " ORDER BY " . $this->_orderBy;
            if(isset($this->_order))
            {
                $extension .= " " . $this->_order;
            }
        }
        
        if(isset($this->_limit))
        {
            $extension .= " LIMIT " . $this->_limit;
            if(isset($this->_limitOffset))
            {
                $extension .= ", " . $this->_limitOffset;
            }
        }
        return $extension;
    }
    
    /**
     * 
     * Adds LIMIT keyword to the statement
     * 
     * Example usage:
     * 
     * $result = $databaseHandler->clients()
     *                           ->select(array("column1", "columnb", "columnc")) 
     *                           ->where(array("id" => array(1,2,3),"columnBeta"=> 1))    //WHERE k_id IN (1, 2, 3) AND columnBeta = 1
     *                           ->limit("50")                                            //LIMIT 50
     *                           ->commit()
     *                           ->fetchAll();
     * 
     * $result = $databaseHandler->clients()
     *                           ->select(array("column1", "columnb", "columnc")) 
     *                           ->where(array("id" => array(1,2,3),"columnBeta"=> 1))    //WHERE k_id IN (1, 2, 3) AND columnBeta = 1
     *                           ->limit("50", "5")                                         //LIMIT 50 , 5
     *                           ->commit()
     *                           ->fetchAll();
     * 
     * @param type $limit
     * @param type $limitOffset
     * @return $this
     */
    
    public function limit($limit, $limitOffset = null)
    {
        $this->_limit = $limit;
        if(isset($limitOffset))
        {
            $this->_limitOffset = $limitOffset;
        }
        return $this;
    }
    
    /**
     * 
     * $result = $databaseHandler->clients()
     *                           ->select(array("column1", "columnb", "columnc")) 
     *                           ->where(array("id" => array(1,2,3),"columnBeta"=> 1))    //WHERE k_id IN (1, 2, 3) AND columnBeta = 1
     *                           ->orderBy("ordercolumn", "ASC")                          //ORDER BY ordercolumn ASC
     *                           ->commit()
     *                           ->fetchAll(); 
     * @param type $paramName
     * @param type $order
     * @return $this
     * @throws \Exception
     */
    
    public function orderBy($paramName, $order = null)
    {
        $this->_orderBy = $paramName;
        if(isset($order))
        {
            if(($order === "ASC") || ($order === "DESC"))
            {
                $this->_orderParam = $order;
            }
            else
            {
                throw new \Exception('DatabaseHandler::orderBy - only ASC or DESC allowed');
            }
        }
        return $this;
    }
    
    
    /**
     * 
     * 
     * $result = $databaseHandler->mytable()
     *                     ->insert(array                //INSERT INTO
     *                             (
     *                              array("column1" => "value1", "column2"=>"value2", "column3"=>"value3"),
     *                              array("column1" => "value4", "k_adres"=>"value5", "k_trud"=>"value6"),
     *                             ))                                      
     *                     ->commit()
     *                     ->rowCount();
     * 
     * 
     * 
     * @param type $insertArray
     * @param type $columns
     * @return $this
     * @throws \Exception
     */
    
    public function insert($insertArray, $columns = null)
    {
        $this->_query = "INSERT INTO " . $this->_table . " ";
        //read column names
        if(isset($insertArray[0]))
        {
            $columns = array_keys($insertArray[0]);
        }
        else
        {
            $this->outputInConsole('Bad input in DatabaseHandler::insert() method. Wrong array format', true);
        }
        $this->_query .= "(" . implode(", ", $columns) . ") VALUES ";
        //iterate through each element and build statement
        $insertRows = array();
        for($i = 0; $i < count($insertArray); $i++)
        {
            $paramNames = array();
            //iterate through each column and add value refences
            for($index = 0; $index < count($columns); $index ++)
            {
                 //check if column exists
                if(isset($insertArray[$i][$columns[$index]]))
                {
                    $this->_bindParamReferences[$columns[$index] . $i] = $insertArray[$i][$columns[$index]];
                }
                else
                {
                    $this->outputInConsole("DatabaseHandler::insert() Column naming does not match first row. Check input array index: " . ($i - 1), true);
                }
                //prepare unique paramnames in pattern  ': + columnName + $i'
                $paramNames[$index] = ":" . $columns[$index] . $i;
            }
            //append statement
            $insertRows[$i] = "(" . implode(", ", $paramNames) . ")";
        }
        $this->_query .= implode(", ",  $insertRows);
        return $this;
    }
    
    /**
     * 
     * badds WHERE keyword to the statement
     * 
     * Example usage:
     * $result = $databaseHandler->clients()
     *                     ->select() 
     *                     ->where(array("id" => array(1,2,3),"column10"=> "something"))    //WHERE id IN (1, 2, 3) AND column10 = something
     *                     ->commit()
     *                     ->fetchAll();
     */
    
    public function where($whereArray)
    {
        $this->_deleteStatementSafety = false;
        $this->_where = array();
        foreach ($whereArray as $column => $value)
        {
            $where="";
            // WHERE PARAM IN (:a, :b, :c)
            if(is_array($value))
            {
                //iterate through each input value and append where IN statement
                $assemblyArray = array();
                for ($x=0; $x < count($value); $x++)
                {
                    array_push($assemblyArray, ":$column" . "$x");
                    $this->_bindParamReferences["$column" . "$x"] = $value[$x];
                }
                $where .= "$column IN (" . implode(", ",$assemblyArray) . ")";
            }
            // WHERE PARAM = :a
            else
            {
                $where .= "$column = :$column";
                $this->_bindParamReferences[$column] = $value;
            }
            array_push($this->_where, $where);
        }
        return $this;
    }
    
    /**
     * 
     * pushes prepared statement with binded params to PDO
     * 
     * @return boolean
     */
    public function commit()
    {
        $this->_query .= $this->getExtension();
        $result = $this->sqlquery($this->_query, $this->_bindParamReferences);
        $this->resetQuerySettings();
        return $result;
    }
    
    /**
     * resets all temp data after query is finished
     */
    public function resetQuerySettings()
    {
        $this->_where = null;
        $this->_query = "";
        $this->_extension = null;
        $this->_bindParamReferences = array();
        $this->_table;
        $this->_orderBy = null;
        $this->_order = null;
        $this->_limit = null;
        $this->_limitOffset = null;
        $this->_columns = array();
    }
    
    
    /**
     * 
     * lets using table() method in another way:
     * 
     * $databaseHandler->tableName()
     *                 ->....()
     * 
     * @param type $tablename
     * @param type $args
     * @return type
     */
    public function __call($tablename, $args)
    {
        return call_user_func_array(array($this, "table"), array($tablename, $args));
    }
}
