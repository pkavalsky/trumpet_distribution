<?php

namespace Trumpet\Database;

class ConnectionManager 
{
    private $_credentialsContainer = array();
    private $_connectionContainer = array();
    public static $_instance;
    
    /**
     * 
     * adds DB credentials to container array where key is dbname
     * accepts 4 parameters $host, $dbname, $user, $password
     * OR
     * 1 parameter - $dbname (in that case reuses credentials which were added earlier)
     * 
     * Example:
     * $connectionManager->addConnection('myhost', 'mydb', 'myuser', 'mypass')
     *                  ->addConnection('mydb2')        //reuses above host/user/pass
     *                  ->addConnection('myOtherHost', 'myAnotherDb','anotheruser','anotherpass')
     * 
     * @param type $first 
     * @param type $second
     * @param type $third
     * @param type $fourth
     * @return $this
     * @throws \Exception
     */
    public function addCredentials($first, $second = null, $third = null, $fourth = null)
    {
        //if db already exists in queue
        if(isset($this->_credentialsContainer[$second]))
        {
            throw new \Exception('ConnectionManager: Connection credentials were already added');
        }
        //if all 4 parameters are there - add them to container
        else if(!empty($first) && isset($second) && isset($third) && isset($fourth))
        {
            $this->_credentialsContainer[$second] = array
            (
                'host' => $first,
                'dbname' => $second,
                'user' => $third,
                'password' => $fourth
            );
        }
        else if(isset($this->_credentialsContainer[$first]))
        {
            throw new \Exception('ConnectionManager: Connection credentials were already added');
        }
        // treat first parameter as dbname and use the the latest added credentials
        else
        {
            $latestCredentials = end($this->_credentialsContainer);
            $this->_credentialsContainer[$first] = array
            (
                'host' => $latestCredentials['host'],
                'dbname' => $first,
                'user' => $latestCredentials['user'],
                'password' => $latestCredentials['password']
            );
        }
        return $this;
    }
    
    /**
     * 
     * initiates and returns PDO connection based on given db name
     * Stores initiated connection in the container
     * if same connection requested couple times - returns the first one
     * 
     * Example:
     * $connectionManager->getConnection('mydb'); //returns PDO connection identified by earlier added DB name
     * ...some code...
     * $connectionManager->getConnection('mydb'); // returns same PDO connection (does not create new one)
     * 
     * 
     * @param type $dbname
     * @return type
     * @throws \Exception
     */
    
    public function getConnection($dbname)
    {
        if(isset($this->_credentialsContainer[$dbname]))
        {
            //fetch credentials
            $credentials = $this->_credentialsContainer[$dbname];
            $host = $credentials['host'];
            $dbname = $credentials['dbname'];
            $user = $credentials['user'];
            $password = $credentials['password'];
            
            //check if connection already exists in connectionContainer
            if(isset($this->_connectionContainer[$dbname]))
            {
                return $this->_connectionContainer[$dbname];
            }
            else
            {
                $connection = new \PDO('mysql:host=' . $host . ';dbname=' . $dbname, $user, $password);
                $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                return $this->_connectionContainer[$dbname] = $connection;
            }
        }
        else
        {
            throw new \Exception('ConnectionManager: couldn\t find requested connection credentials');
        }
    }
    
    /**
     * 
     * Initiates ConnectionManager instance
     * $connectionManager = ConnectionManager::getInstance();
     * $connectionManager->somemethod()...;
     * @return type
     */
    
    public static function getInstance()
    {
        if(isset(self::$_instance))
        {
            return self::$_instance;
        }
        self::$_instance = new ConnectionManager;
        return self::$_instance; 
    }
}
