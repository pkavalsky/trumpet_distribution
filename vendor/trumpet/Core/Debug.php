<?php

namespace Trumpet\Core;
use Trumpet\Core\Controller;

class Debug extends Controller
{
    public static function exception($message)
    {
        $errormsg = "<p>Unexpected exception: $message </p>";
        require_once "debugviews/debug_view.php";
    }
}
