<?php

namespace Trumpet\Core;
use Trumpet\Core\Debug;
use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Router
{
    private $_controller;
    private $_action;
    
    private $_request;
    
    private function __construct(Request $request)
    {
        $this->_request = $request;
        $this->setController();
        $this->setAction();
    }
    
    private function setController()
    {
        if($this->_request->isControllerSet())
        {
            //iterate through each directory
            $dir_iterator = new RecursiveDirectoryIterator(CONTROLLERS_DIR);
            $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);
            foreach ($iterator as $file) 
            {
                //delete full path to Controllers dir
                $file = str_replace(CONTROLLERS_DIR, "", "$file");
                //remove .php extension
                $file = str_replace(".php", "", $file);
                //remove namespace part and leave only filename with extension
                $filename = explode("\\", $file);
                $filename = end($filename);
                //compare case insensitive filename and controller name from URL
                if(strtolower($filename) === (strtolower($this->_request->getController())))
                {
                    //replace directory separators with namespace separator
                    $file = str_replace(DIRECTORY_SEPARATOR, "\\", $file);
                    //add default controller namespace
                    $this->_controller = "App\\Controllers\\" . $file;
                    break;
                }
            }
        }
        else
        {
            $this->_controller = "App\\Controllers\\" . DEFAULT_CONTROLLER;
        }
    }
    
    private function setAction()
    {
        if($this->_request->isActionSet())
        {
            $this->_action = $this->_request->getAction();
        }
        else
        {
            $this->_action = "indexAction";
        }
    }
    
    private function dispatch()
    {
        if(class_exists($this->_controller))
        {
            $controller = new $this->_controller();
            if(method_exists($this->_controller, $this->_action))
            {
                $controller->beforeAction($this->_request);
                $controller->{$this->_action}($this->_request);
                $controller->afterAction($this->_request);
            }
            else
            {
                throw new Exception("Action <strong>$this->_action</strong> not found in <strong>$this->_controller</strong>");
            }
        }
        else
        {
            throw new Exception("Controller <strong> " . $this->_request->getController() . " </strong> not found in controllers directory");
        }
    }
    
    private function notFoundError()
    {
        $this->_controller = "App\\Controllers\\" . DEFAULT_CONTROLLER;
        $controller = new $this->_controller;
        $controller->errorAction();
    }
    
    public static function run($request)
    {
        try
        {
            $object = new Router($request);
            $object->dispatch();
        }
        catch (Exception $e)
        {
            //for development env inform about exception
            if(ENVIRONMENT === "development")
            {
                Debug::exception($e->getMessage());
            }
            //for production redirect to root if config says so
            else if(ROUTE_TO_ROOT)
            {
                header("Location: /");
            }
            //display not found / error page
            else
            {
                $object->notFoundError();
            }
            
        }
    }
}
