<?php
namespace Trumpet\Core;
use Trumpet\Core\ConfReader;
use Trumpet\Database\ConnectionManager;

class Framework 
{    
    private function __construct() 
    {
        //go 3 directories up and enter src dir
        $rootpath = __DIR__ . str_repeat(DIRECTORY_SEPARATOR . "..", 3);
        $rootpath = realpath($rootpath) . DIRECTORY_SEPARATOR;
        define("PROJECT_ROOT_PATH", $rootpath);
        $srcpath =  $rootpath . "src" . DIRECTORY_SEPARATOR;
        //absolute path to root dir of project
        $srcpath = realpath($srcpath) . DIRECTORY_SEPARATOR;
        $timezone = ConfReader::initiate("time");
        date_default_timezone_set($timezone["zone"]);
        $settings = ConfReader::initiate('general');
        //load settings to global variables
        define("ENVIRONMENT", $settings['environment']); 
        define("DEFAULT_CONTROLLER", $settings['default_controller']);
        define("ERROR_VIEW", $settings['error_view']);
        define("ROUTE_TO_ROOT", $settings['route_to_root']);
        define("VIEWS_DIR", $srcpath . "views" . DIRECTORY_SEPARATOR);
        define("CONTROLLERS_DIR", $srcpath . "controllers" . DIRECTORY_SEPARATOR);
        define("MODELS_DIR", $srcpath . "models" . DIRECTORY_SEPARATOR);
        $dbsettings = ConfReader::initiate('database');
        //get db info
        define("DB_HOST", $dbsettings['host']);
        define("DB_NAME", $dbsettings['database']);
        define("DB_USER", $dbsettings['user']);
        define("DB_PASSWORD", $dbsettings['password']);
        $connectionManager = ConnectionManager::getInstance();
        $connectionManager->addCredentials(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD);
        $nextDatabase = ConfReader::initiate('database1');
        if (isset($nextDatabase))
            $this->databaseDetailsIterator($nextDatabase, $connectionManager);
        //set error reporting if env is development
        if(ENVIRONMENT === "development")
        {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }
        //start session
        if(!isset($_SESSION))
        {
            session_start();
        }
    }
    
    private function databaseDetailsIterator($nextDatabase, ConnectionManager $connectionManager)
    {
        $dbSections = array();
        $dbSections[] = $nextDatabase;
        for($i=2;$i<$i+1;$i++)
        {
            $nextDb = $nextDatabase = ConfReader::initiate("database$i");
            if(isset($nextDb))
            {
                $dbSections["database$i"] = $nextDb;
                continue;
            }
            break;
        }
        $x=1;
        foreach($dbSections as $dbsection => $dbSection)
        {
            //allowed JUST database without any other parameters - will use host and username & password from earlier db
            if(isset($dbSection["dbname"]) &&
               !isset($dbSection["user"]) &&
               !isset($dbSection["password"]) &&
               !isset($dbSection["host"]))
            {
                define("DB_NAME$x", $dbSection["dbname"]);
                $connectionManager->addCredentials($dbSection["dbname"]);
            }
            //need host, user, password, dbname
            else if(isset($dbSection["host"]) && 
               isset($dbSection["user"]) &&
               isset($dbSection["password"]) &&
               isset($dbSection["dbname"]))
            {
                define("DB_NAME$x", $dbSection["dbname"]);
                define("DB_HOST$x", $dbSection["host"]);
                define("DB_USER$x", $dbSection["user"]);
                define("DB_PASSWORD$x", $dbSection["password"]);
                $connectionManager->addCredentials(
                    $dbSection["host"],
                    $dbSection["dbname"],
                    $dbSection["user"],
                    $dbSection["password"]
                );
            }
            else
                throw new \Exception("Unrecognized database details entry in config in section $dbsection");
            $x++;
        }
    }
    
    private function startUpRegistry()
    {
        $startupRegistryConfig = ConfReader::initiate("StartupRegistry");
        if(!empty($startupRegistryConfig))
        {
            foreach ($startupRegistryConfig as $classToLaunch)
            {
                if(class_exists($classToLaunch))
                {
                    $object = new $classToLaunch;
                }
                else
                    throw new \Exception("Class with name $classToLaunch in [StartupRegistry] not existing");
            }
        }
    }
    
    public static function init()
    {
        new Framework;
    }
    //put your code here
}
