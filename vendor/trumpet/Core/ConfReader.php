<?php

namespace Trumpet\Core;
class ConfReader
{
    //conf locations
    private static $configdir = array('../config/conf.ini');
    private $_ini;
    private static $instance;

    private function __construct()
    {  
        foreach (self::$configdir as $confpath) 
        {
            $validate_path = file_exists($confpath);
            if($validate_path === true)
            {
                $tempini =  parse_ini_file($confpath, TRUE);
                $this->_ini = $tempini;
                return;
            }
        }
        throw new Exception('Can\'t find config file');
    }
    
    public function getSection($confSection)
    {
        if(isset($this->_ini[$confSection]))
        {
            return $this->_ini[$confSection];
        }
        return null;
    }
    //singleton pattern
    public static function initiate($confSection)
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance->getSection($confSection);
    }
}
