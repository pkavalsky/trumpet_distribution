<?php

namespace Trumpet\Core\Forms;
use Trumpet\Core\Request;
use Exception;

class FormMaker

{
    private $_model;
    private $_fields;
    private $_validationStatus;
    private $_fieldInputValues;
    private $_request;
    private $_method;
    
    public function __construct($model)
    {
        $this->_model = $model;
    }
    
    public function add($field, $validateObject)
    {
        //methods in model need to have 'getFieldname()' / 'setFieldname()' convention
        $getter = "get" . ucfirst($field);
        $setter = "set" . ucfirst($field);
        //check if getter exists
        if(!method_exists($this->_model, $getter))
        {
            throw new Exception("Couldn't find getter $getter in model " . get_class($this->_model));
        }
        //check if setter exists
        if(!method_exists($this->_model, $setter))
        {
            throw new Exception("Couldn't find setter $setter in model " . get_class($this->_model));
        }
        //load field name and validation class into array
        $this->_fields[$field] = $validateObject;
        return $this;
    }
    
    //validates request fields and loads all statuses into validateStatus array
    public function handleRequest(Request $request, $method = "POST")
    {
        $this->_method = $method;
        $this->_request = $request;
        foreach ($this->_fields as $fieldName => $validateObject)
        {
            if($this->_method === "POST")
            {
                $fieldValue = $this->_request->getPost($fieldName);
            }
            else if ($this->_method === "GET")
            {
                $fieldValue = $this->_request->getGet($fieldName);
            }
            else if ($this->_method === "URL")
            {
                $fieldValue = $this->_request->getParam($fieldName);
            }
            else
            {
                throw new Exception("Unknown method in handleRequest()");
            }
            $status = $validateObject->validate($fieldValue);
            $this->_validationStatus[$fieldName] = $status;
            $this->_fieldInputValues[$fieldName] = $fieldValue;
        }
    }
    
    //checks if single field / fields are valid
    public function isValid($fieldName = null)
    {
        //check validation for single field
        if(!($fieldName === null))
        {
            if(!isset($this->_validationStatus[$fieldName]))
            {
                throw new Exception("this field is not present in the submitted form");
            }
            return $this->_validationStatus[$fieldName];
        }
        //check validation for all fields
        foreach ($this->_validationStatus as $field => $status)
        {
            if($status)
            {
                continue;
            }
            else
            {
                return false;
            }
        }
        return true;
    }
    
    //checks if all fields have been submitted
    public function isSubmitted($fieldName = null)
    {
        //check single field
        if(!($fieldName === null))
        {
            return $this->_request->checkPost($fieldName);
        }
        //check all fields
        foreach ($this->_fields as $field => $validateObject)
        {
            if($this->_method === "POST")
            {
                if($this->_request->checkPost($field))
                {
                    continue;
                }
            }
            else if($this->_method === "GET")
            {
                if($this->_request->checkGet($field))
                {
                    continue;
                }
            }
            else if($this->_method === "URL")
            {
                if($this->_request->checkParam($field))
                {
                    continue;
                }
            }
            else 
            {
                throw new Exception("Requested method not found");
            }
            return false;
            
        }
        return true;
    }
    
    //loads data into model
    public function getData()
    {
        foreach($this->_fieldInputValues as $fieldName => $fieldValue)
        {
            $setterName = "set" . ucfirst($fieldName);
            $this->_model->{$setterName}($fieldValue);
        }
        return $this->_model;
    }
}
