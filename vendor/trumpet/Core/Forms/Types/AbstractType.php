<?php
namespace Trumpet\Core\Forms\Types;

abstract class AbstractType 
{
    public static function init()
    {
        $className = get_called_class();
        return new $className;
    }
}
