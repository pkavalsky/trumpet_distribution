<?php
namespace Trumpet\Core\Forms\Types;
use Trumpet\Core\Forms\Types\AbstractType;

class NumberType extends AbstractType
{
    public function validate($number)
    {
        return $this->_number = is_numeric($number);
    }
}
