<?php
namespace Trumpet\Core\Forms\Types;
use Trumpet\Core\Forms\Types\AbstractType;

class EmailType extends AbstractType
{
    public function validate($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            return $this->_email = true;
        }
        else
        {
            return $this->_email = false;
        }
    }
}