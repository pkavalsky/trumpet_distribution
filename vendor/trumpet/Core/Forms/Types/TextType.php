<?php
namespace Trumpet\Core\Forms\Types;
use Trumpet\Core\Forms\Types\AbstractType;

class TextType extends AbstractType
{
    private $_string;
    
    public function validate($string)
    {
        return $this->_string = is_string($string);
    }
}
