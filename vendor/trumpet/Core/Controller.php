<?php

namespace Trumpet\Core;
use Trumpet\Core\Forms\FormMaker;
use Exception;
use Trumpet\Core\Registry;

abstract class Controller
{
    protected $_twig = null;
    
    protected function render($view, $params = null)
    {
        if(!($params === null) && !is_array($params))
        {
            throw new Exception("When calling render() parameters need to be passed as associative array in " . get_called_class());
        }
        if(is_null($params))
        {
            $params = array();
        }
        $twig = $this->getTwigEnvironment();
        echo $twig->render($view, $params);
    }
    
    protected function renderToVar($view, $params = null)
    {
        if(!($params === null) && !is_array($params))
        {
            throw new Exception("When calling render() parameters need to be passed as associative array in " . get_called_class());
        }
        if(is_null($params))
        {
            $params = array();
        }
        $twig = $this->getTwigEnvironment();
        return $twig->render($view, $params);
    }
    
    protected function getTwigEnvironment()
    {
        if(isset($this->_twig))
        {
            return $this->_twig;
        }
        $loader = new \Twig_Loader_Filesystem(VIEWS_DIR);
        return  $this->_twig = new \Twig_Environment($loader);
    }
    
    protected function redirect($controller, $action = null)
    {
        if(!isset($action))
        {
            header("Location: /$controller", true, 301);
            // header("Location: /$controller");
        }
        else
        {
            header("Location: /$controller/$action/", true, 301);
            // header("Location: /$controller/$action/");
        }
        exit();
    }
    
    //required in each controller
    public function indexAction(Request $request)
    {
        throw new Exception("missing indexAction() in " . get_called_class());
    }
    
    //in conf ini the error_view in views folder
    public function errorAction()
    {
        $this->render(ERROR_VIEW);
    }
    
    public function beforeAction(Request $request) { }
    
    public function afterAction(Request $request) { }
    
    protected function createFormMaker($model)
    {
        return new FormMaker($model);
    }
    
    protected function registry()
    {
        return Registry::initiate();
    }
}
