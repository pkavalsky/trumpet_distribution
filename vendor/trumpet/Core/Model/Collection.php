<?php

namespace Trumpet\Core\Model;

use Trumpet\Core\Model\Entity;

abstract class Collection implements \Iterator
{
    protected $_mapper;
    protected $_total = 0;
    protected $_raw = array();
    private $_pointer = 0;
    private $_objects = array();
    
    /**
     * takes raw array of data and mapper object
     * 
     * @param array $raw
     * @param \Trumpet\Core\Model\Mapper $mapper
     */
    public function __construct(array $raw, Mapper $mapper = null) 
    {
        if(!is_null($raw) && !is_null($mapper))
        {
            $this->_raw = $raw;
            $this->_total= count($raw);
        }
        $this->_mapper = $mapper;
    }
    
    /**
     * adds object into objects array
     * 
     * @param Entity $object
     * @throws Exception
     */
    protected function add(Entity $object)
    {
        $class = $this->targetClass();
        if(! ($object instanceof $class))
        {
            throw new Exception("Wrong input to collection $class");
        }
        $this->_objects[$this->_total] = $object;
        $this->_total++;
    }
    
    abstract function targetClass();
    
    /**
     * takes raw row of data and maps it into an Entity object using mapper
     * 
     * @param type $num
     * @return type
     */
    protected function getRow($num)
    {
        if($num >= $this->_total || $num < 0)
        {
            return null;
        }
        if(isset($this->_objects[$num]))
        {
            return $this->_objects[$num];
        }
        if(isset($this->_raw[$num]))
        {
            $this->objects[$num] = $this->_mapper->createEntity($this->_raw[$num]);
            return $this->objects[$num];
        }
    }
    
    /**
     * move cursor to beginning of the list
     */
    public function rewind()
    {
        $this->_pointer = 0;
    }
    
    /**
     * return object from the current cursor position
     * 
     * @return type
     */
    public function current()
    {
        return $this->getRow($this->_pointer);
    }
    
    /**
     * return current cursor position
     * 
     * @return type
     */
    public function key()
    {
        return $this->_pointer;
    }
    
    /**
     * return object from the current cursor position and move cursor to next
     * 
     * @return type
     */
    public function next()
    {
        $row = $this->getRow($this->_pointer);
        if($row)
        {
            $this->_pointer++;
        }
        return $row;
    }
    
    /**
     * confirm that that object exists on the current pointer position
     * 
     * @return type
     */
    public function valid()
    {
        return (! is_null($this->current()));
    }
    
    /**
     * move pointer to desired position
     * 
     * @param type $pointer
     */
    public function move($pointer)
    {
        $this->_pointer = $pointer;
    }
}
