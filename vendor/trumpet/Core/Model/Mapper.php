<?php

namespace Trumpet\Core\Model;

use Trumpet\Database\ConnectionManager;
use Trumpet\Database\DatabaseAdapter;

abstract class Mapper
{
    protected function getDatabaseAdapter()
    {
        $connectionManager = ConnectionManager::getInstance();
        $pdo = $connectionManager->getConnection(DB_NAME);
        return DatabaseAdapter::getInstance(DB_NAME, $pdo);
    }
}
