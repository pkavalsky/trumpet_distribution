<?php

namespace Trumpet\Core;

class Session
{
    private static $_instance;
    
    public static function getInstance()
    {
        if(isset(self::$_instance))
        {
            return self::$_instance;
        }
        return self::$_instance = new Session;
    }
    
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
    
    public function get($key)
    {
        if (isset($_SESSION[$key]))
        {
            return $_SESSION[$key];
        }
        else
        {
            return false;
        }
    }
    
    public function all()
    {
        return $_SESSION;
    }
    
    public function has($key)
    {
        if (isset($_SESSION[$key]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function remove($key)
    {
        unset($_SESSION[$key]);
    }
}

