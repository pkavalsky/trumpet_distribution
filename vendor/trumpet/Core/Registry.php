<?php

namespace Trumpet\Core;

use Trumpet\Core\ConfReader;
use Exception;

class Registry
{
    private $_servicesRegistry = array();
    private $_serviceObjectsRegistry = array();
    private static $instance;
    
    public function __construct()
    {
        //get all registered services
        $settings = ConfReader::initiate('Registry');
        $this->_servicesRegistry = $settings;
    }
    
    public function get($service)
    {
        //check if service is registered
        if(!isset($this->_servicesRegistry[$service]))
        {
            throw new Exception("Service $service not found in service registry (");
        }
        //check if service object was already created if yes return it
        if(isset($this->_serviceObjectsRegistry[$service]))
        {
            return $this->_serviceObjectsRegistry[$service];
        }
        //register service object and return it
        else
        {
            $this->_serviceObjectsRegistry[$service] = new $this->_servicesRegistry[$service];
            return $this->_serviceObjectsRegistry[$service];
        }
    }
    
    public static function initiate()
    {
        //singleton pattern
        if(!isset(self::$instance))
        {
            return self::$instance = new Registry();
        }
        else
        {
            return self::$instance;
        }
    }
}
