<?php

namespace Trumpet\Core;

class Request 
{
    
    private $_controller;
    private $_controllerExists = false;
    private $_action;
    private $_actionExists = false;
    private $_url;
    private $_params;
    private $_paramsExists = false;
    private $_method;
    private $_post;
    private $_get;
    private $_stream;
    private $_headers = array();
    
    public function __construct()
    {
        if (isset($_SERVER['REQUEST_URI']))
        {
            $this->setBaseUrl();
        }
        $this->setController();
        $this->setAction();
        $this->setParams();
        $this->setMethod();
        $this->setHeaders();
        $this->setStream();
        $this->setGet();
        $this->setPost();
    }
    
    private function setStream()
    {
        $this->_stream = file_get_contents('php://input');
    }
    
    public function getStream()
    {
        return $this->_stream;
    }
    
    private function setHeaders()
    {
        $this->_headers = getallheaders();
    }
    
    public function getHeaders()
    {
        return $this->_headers;
    }
    
    private function setBaseUrl()
    {
        $url = explode('/',rtrim($_SERVER['REQUEST_URI'], '/'));
        for($n = 0; $n < count($url); $n++)
        {
            $this->_url[$n] = htmlspecialchars($url[$n]);
        }
    }
    
    private function setController()
    {
        //check if controller field in url is present
        if (isset($this->_url[1]))
        {
            $this->_controller = $this->_url[1];
            $this->_controllerExists = true;
        }
    }
    
    private function setPost()
    {
        if (isset($_POST))
        {
            $this->_post = $_POST;
        }
        else
        {
            return false;
        }
    }
    
    private function setGet()
    {
        if (isset($_GET))
        {
            $this->_get = $_GET;
        }
        else
        {
            return false;
        }
    }
    
    private function setAction()
    {
        //check if action is present in url
        if (isset($this->_url[2]))
        {
            $this->_action = strtolower($this->_url[2]) . "Action";
            $this->_actionExists = true;
        }
    }
    
    private function setParams()
    {
        if (count($this->_url > 3))
        {
            $this->_paramsExists = true;
            for($n = 3; $n < count($this->_url);$n+=2) 
            {
                $key = $this->_url[$n];
                //add key / value pairs to associative array
                if(isset($this->_url[$n + 1]))
                {
                    $value = $this->_url[$n + 1];
                    $this->_params[$key] = $value;
                }
                //push at the end of array with default php digit as key
                else
                {
                    $this->_params[count($this->_params)] = $key;
                }
            }
        }
        else
        {
            $this->_params = false;
        }
    }
    
    public function setMethod()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {
            $this->_method = "POST";
        }
        else
        {
            $this->_method = "GET";
        }
    }
    
    public function getMethod()
    {
        return $this->_method;
    }
    
    public function getBaseUrl()
    {
        return $this->_url;
    }
    
    public function getController()
    {
        return $this->_controller;
    }
    
    public function getAction()
    {
        return $this->_action;
    }
    
    public function getIP()
    {
	return $_SERVER['REMOTE_ADDR'];
    }
    
    public function getUserAgent()
    {
	return $_SERVER['HTTP_USER_AGENT'];
    }
    
    public function getPost($postfield, $htmlfiltered = null)
    {   
        if (isset($this->_post[$postfield]) && $htmlfiltered)
        {
            return htmlspecialchars($this->_post[$postfield]);
        }
        else if (isset($this->_post[$postfield]))
        {
            return $this->_post[$postfield];
        }
        else
        {
            return false;
        }
    }
    
    public function getGet($getfield, $htmlfiltered = null)
    {
        if (isset($this->_get[$getfield]) && $htmlfiltered)
        {
            return htmlspecialchars($this->_get[$getfield]);
        }
        else if (isset($this->_get[$getfield]))
        {
            return $this->_get[$getfield];
        }
        else
        {
            return false;
        }
    }
    
    public function getParam($paramfield, $htmlfiltered = null)
    {
        if(isset($this->_params[$paramfield]) && $htmlfiltered)
        {
            return htmlspecialchars($this->_params[$paramfield]);
        }
        else if(isset($this->_params[$paramfield]))
        {
            return $this->_params[$paramfield];
        }
        else
        {
            return false;
        }
    }
    
    public function checkGet($getfield)
    {
        if (isset($_GET[$getfield]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function checkPost($postfield)
    {
        if (isset($_POST[$postfield]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function checkParam($paramfield)
    {
        if(isset($this->_params[$paramfield]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function isControllerSet()
    {
        return $this->_controllerExists;
    }
    
    public function isActionSet()
    {
        return $this->_actionExists;
    }
    
    public function isParamsSet()
    {
        return $this->_paramsExists;
    }

    public static function getInstance()
    {
        return new Request;
    }
    
    public function isAjax()
    {
        $headers = $this->getHeaders();
        if( isset($headers["X-Requested-With"]) && 
            $headers["X-Requested-With"] === "XMLHttpRequest") 
            return true;
        else
            return false;
    }
}
