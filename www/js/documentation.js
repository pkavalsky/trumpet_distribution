function Documentation()
{
    this.section = 'default';
    this.details = "none_selected";
    this.scrollToMethod = "none_selected";
    this.setScrollToMethod = function (scrollMethod)
    {
        this.scrollToMethod = scrollMethod;
    };
    this.setDetails= function (details)
    {
        this.details = details;
    };
    this.getSection = function()
    {
        return this.section;
    };

    this.setSection = function(section)
    {
        this.section = section;
        
    };
    this.callForContent = function() 
    {
        $.ajax(
        {
            scrollToMethod: this.scrollToMethod,
            type: 'POST',
            url: '/documentation/fetch/',
            //send as json
            contentType: 'application/json',
            data: JSON.stringify(
                    {'section': this.section, 'details': this.details}
                    ),
            dataType: 'html',
            cache: false,
            timeout: 7000,
            success: function(data) 
            {
                $('#content').hide().html(data).fadeIn();
                Prism.highlightAll();
            },
            complete: function ()
            {
                //scroll to requested method
                if(this.scrollToMethod !== "none_selected")
                {
                    console.log("przeszlo");
                    console.log(this.scrollToMethod);
                    $('html, body').animate({
                        scrollTop: $("#" + this.scrollToMethod).offset().top - 80
                    }, 1000);
                }
            },
            beforeSend: function()
            {
                $('#content').html('<center><img style="height:60px;width:60px;margin-top:40%" src="/img/loading.gif"></center>');
            },
            error: function () 
            {
                $('#content').html('<div class="alert alert-danger"><strong>Error:</strong> there was a problem with connection to the server</strong></div>');
            }
        });
    };
};

function Search ()
{
    this.phrase;
    this.index;
    this.setPhrase = function (phrase)
    {
        this.phrase = phrase;
    };
    this.getPhrase = function ()
    {
        return this.phrase;
    };
    this.setSearchIndex = function (index)
    {
        if(index === 'class')
        {
            this.index = "class";
        }
        else if (index === "method")
        {
            this.index = "method";
        }
        else
        {
            this.index = "error";
        }
    };
    this.callForResults = function()
    {
        $.ajax(
        {
            type: 'POST',
            url: "/documentation/search/index/" + this.index + "/",
            //send as json
            contentType: 'application/json',
            data: JSON.stringify({'phrase': this.getPhrase(), 'limit': this.limit}),
            dataType: 'html',
            cache: false,
            timeout: 7000,
            success: function(data) 
            {
                $('#tablecontent').hide().html(data).fadeIn();
            },
            error: function () 
            {
                $('#tablecontent').html('<div class="alert alert-danger"><strong>Error:</strong> there was a problem with connection to the server</strong></div>');
            }
        });
    };
};
