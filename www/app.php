<?php
require_once "../autoload.php";
use Trumpet\Core\Framework;
use Trumpet\Core\Router;
use Trumpet\Core\Request;

Framework::init();
$request = Request::getInstance();
Router::run($request);




