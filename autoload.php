<?php
require_once 'vendor/trumpet/Core/AutoLoader.php';
require_once 'vendor/autoload.php';

use Trumpet\Core\AutoLoader;

// instantiate the loader
$loader = new AutoLoader;

// register the autoloader
$loader->register();

// register the base directories for the namespace prefix
$loader->addNamespace('App\\Controllers', __DIR__ . '/src/controllers');
$loader->addNamespace('App\\Models', __DIR__ . '/src/models');
$loader->addNamespace('App\\Services', __DIR__ . '/src/services');
$loader->addNamespace('Trumpet', __DIR__ . '/vendor/trumpet');
