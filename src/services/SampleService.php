<?php

namespace App\Services;

//service which reads, decodes json files and searches for results by id
class SampleService 
{
    private $_storage;
    
    public function __construct()
    {
        $storage_dir = PROJECT_ROOT_PATH . "storage" . DIRECTORY_SEPARATOR . "samplestorage.json";
        $this->_storage = file_get_contents($storage_dir);
    }
    
    public function find($id)
    {
        $data = json_decode($this->_storage, true);
        foreach($data as $row)
        {
            if($row['id'] == $id)
            {
                return $row;
            }
        }
        return null;
    }
    
    public function fetchAll()
    {
        return $data = json_decode($this->_storage, true);
    }
}
