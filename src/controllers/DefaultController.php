<?php

namespace App\Controllers;

use Trumpet\Core\Controller;
use Trumpet\Core\Request;
use Trumpet\Core\Session;
use App\Models\SampleModel\SampleMapper;
use App\Services\SampleService;

class DefaultController extends Controller 
{
    public function indexAction(Request $request)
    {
        $storage = new SampleService;
        $mapper = new SampleMapper($storage);
        $entity = $mapper->findById(1);
        $entityCollection = $mapper->fetchAll();
        //move cursor from 0 to 1
        $entityCollection->move(1);
        while($entityCollection->valid())
        {
            $entities[] = $entityCollection->next();
        }

        $this->render("home.twig", array(
            "entity"=>$entity,
            "entities" => $entities       
        ));
    }
}
