<?php

namespace App\Controllers;

use Trumpet\Core\Controller;
use Trumpet\Core\Request;

use App\Models\Playground\Item;
use App\Models\Playground\Items;

class Playground extends Controller 
{
    public function indexAction(Request $request)
    {
        $apple = new Item("Apple", 1);
        $banana = new Item("Banana", 2);
        $bottle = new Item("Bottle", 3);
        
        $bucket = new Items("Bucket", 2);
        
        $bucket->add($apple)
               ->add($banana)
               ->add($bottle);
        
        
        $computer = new Item("Computer", 10);
        $gum = new Item("Gum", 2);
        
        $bag = new Items("Bag", 3);
        $bag->add($gum)
            ->add($computer);
        
        $bucket->add($bag);
        
        
        $bucket->remove("Bag");
        echo $bucket->getValue();
        
        
        
        
        
        
        
    }
}
