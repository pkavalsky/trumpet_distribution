<?php

namespace App\Models\SampleModel;

use Trumpet\Core\Model\Entity;

class SampleEntity extends Entity
{
    private $_title;
    private $_msg;
    private $_link;
    
    public function getTitle()
    {
        return $this->_title;
    }
    
    public function getMessage()
    {
        return $this->_msg;
    }
    
    public function getLink()
    {
        return $this->_link;
    }
    
    public function setTitle($title)
    {
        return $this->_title = $title;
    }
    
    public function setMessage($message)
    {
        return $this->_msg = $message;
    }
    
    public function setLink($link)
    {
        return $this->_link = $link;
    }
}
