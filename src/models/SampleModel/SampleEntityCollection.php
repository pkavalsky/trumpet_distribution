<?php

namespace App\Models\SampleModel;

use Trumpet\Core\Model\Collection;

class SampleEntityCollection extends Collection
{
    public function targetClass()
    {
        return "App\Model\SampleModel\Entity";
    }
}
