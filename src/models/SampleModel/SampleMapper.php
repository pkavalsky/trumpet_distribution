<?php

namespace App\Models\SampleModel;

use Trumpet\Core\Model\Mapper;
use App\Models\SampleModel\SampleEntity;
use App\Models\SampleModel\SampleEntityCollection;

class SampleMapper extends Mapper
{
    private $_storageAdapter;
    
    public function __construct($storageAdapter)
    {
        $this->_storageAdapter = $storageAdapter;
        //if mapper will be using database storage..
        //$this->_dbh = $this->getDatabaseAdapter();   
    }
    
    public function findById($id)
    {
        $result = $this->_storageAdapter->find($id);
        if($result)
        {
            $entity = new SampleEntity;
            $this->mapResultToEntity($result, $entity);
            return $entity;
        }
    }
    
    public function fetchAll()
    {
        $result = $this->_storageAdapter->fetchAll();
        return new SampleEntityCollection($result, $this);
    }

    public function mapResultToEntity(array $result, SampleEntity $entity)
    {
        $entity->setTitle($result["title"]);
        $entity->setMessage($result["message"]);
        $entity->setLink($result["link"]);
    }
    
    public function createEntity(array $result)
    {
        $entity = new SampleEntity;
        $this->mapResultToEntity($result, $entity);
        return $entity;
    }
}
