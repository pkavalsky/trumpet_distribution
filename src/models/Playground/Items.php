<?php

namespace App\Models\Playground;

use App\Models\Playground\ItemInterface;

class Items implements ItemInterface
{
    private $_itemsCount = 0;
    private $_itemValue;
    private $_itemName;
    private $_items = array();
    
    public function __construct($itemName, $itemValue) 
    {
        $this->_itemName = $itemName;
        $this->_itemValue = $itemValue;
    }
    
    public function getCount()
    {
        return $this->_itemsCount;
    }
    
    public function getValue()
    {
        return $this->_itemValue;
    }
    
    
    public function add(ItemInterface $item) 
    {
        if(!isset($this->_items[$item->getName()]))
        {
            $this->_items[$item->getName()] = $item;
            $this->_itemValue += $item->getValue();
            $this->_itemsCount++;
        }
        else
        {
            throw new \Exception("Item " . $item->getName() . " already exists in " . $this->getName() . " you cannot add it again ");
        }
        return $this;
    }
    
    public function remove($itemName) 
    {
        if(isset($this->_items[$itemName]))
        {
            $this->_itemValue -= $this->_items[$itemName]->getValue();
            unset($this->_items[$itemName]);
            $this->_itemsCount--;
        }
        else
        {
            throw new \Exception("Item " . $itemName . " does not exist in " . $this->getName() . " so you cannot remove it");
        }
    }
    
    public function getName()
    {
        return $this->_itemName;
    }
    
    public function get()
    {
        return $this->_items;
    }
}
