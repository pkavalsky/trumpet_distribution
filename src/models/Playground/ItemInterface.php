<?php

namespace App\Models\Playground;

interface ItemInterface
{
    function __construct($itemName, $itemValue);
    function getValue();
    function getName();
    function getCount();
    function add(ItemInterface $item);
    function remove($item);
}
