<?php

namespace App\Models\Playground;

use App\Models\Playground\ItemInterface;

class Item implements ItemInterface
{
    private $_itemName;
    private $_itemValue;
    
    public function __construct($itemName, $itemValue) 
    {
        $this->_itemName = $itemName;
        $this->_itemValue = $itemValue;
    }
    
    public function getCount()
    {
        return 1;
    }
    
    public function add(ItemInterface $item) 
    {
        throw new \Exception("Cannot add items to $this->_itemName");
    }
    
    public function remove($item) 
    {
        throw new \Exception("Cannot remove items from $this->_itemName");
    }
    
    public function getName()
    {
        return $this->_itemName;
    }
    
    public function getValue()
    {
        return $this->_itemValue;
    }
}
